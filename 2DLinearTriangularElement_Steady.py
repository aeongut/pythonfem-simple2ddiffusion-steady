# Author:	Alp Emre ONGUT
# Date:		05.11.2012
# Abstract:	This is a simple Python script which is written using scipy and sympy
#			and which solves the 2D heat diffusion problem using linear triangular elements.

#Python modules are being loaded here.
import numpy as np
import scipy as sp               #scientific python module
import sympy as sym              #symbolic python module
import matplotlib.pyplot as plt  #plotting module

#Mesh properties 
ne = 5	#number of elements
nn = 7	#number of nodes
nen = 3	#number of element nodes
nsd = 2	#number of space dimentions

#Global stiffness matrix
K = sym.zeros((nn,nn))

#symbolic variables
k = sym.Symbol('k')		#diffusion coefficient
xi = sym.Symbol('xi')	#xi variable
eta = sym.Symbol('eta')	#eta variable

#Connectivity array
mien = sp.mat([ [0,1,6],
				[1,5,6],
				[1,2,5],
				[2,4,5],
				[2,3,4]])

#Coordinate array
mxyz = sp.mat([ [0.0, 0.0],
				[2.0, 0.0],
				[4.0, 0.0],
				[6.0, 0.0],
				[4.0, 2.0],
				[2.0, 2.0],
				[0.0, 2.0]])

#Shape functions linear triangle master element
S = sym.Matrix([[1.0 - xi - eta], [xi], [eta]])

#Derivatives of shape functions wrt xi and eta
dS = sp.mat([ [-1.0, 1.0, 0.0],
   		      [-1.0, 0.0, 1.0]]) 

#Loop for determining the element stiffness matrix and
#assembling the global stiffness matrix
for ie in range(ne):
	#point coordinates in matrix form
	xy = sym.zeros((nen,nsd))
	for ien	in range(nen):
		xy[ien,:] = mxyz[mien[ie,ien],:]

	#Jacobian
	J = dS*xy

	#Derivatives of shape functions wrt x and y
	dSxy = J.inv()*dS

	#Integrand of the element stiffness:
	intStif = k * dSxy.T * dSxy * J.det()

	#Element stiffness matrix is integrated symbolically:
	Ke = sym.integrate(intStif, (eta, 0, 1-xi)).evalf()
	Ke = sym.integrate(Ke, (xi, 0, 1)).evalf()
	print ie
	print Ke
	
	#Assembly in the global system
	for ien in range(nen):
		for jen in range(nen):
			K[mien[ie,ien],mien[ie,jen]] += Ke[ien,jen]

print K

